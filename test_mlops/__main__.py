from test_mlops import cli, model, preprocessing, vectorize  # noqa: F401
from clearml.automation.controller import PipelineDecorator

if __name__ == "__main__":
    PipelineDecorator.run_locally()
    cli.cli_clearml()
