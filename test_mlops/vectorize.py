import polars as pl
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
import torch
from transformers import AutoModel, AutoTokenizer
from torch.utils.data import DataLoader


def train_vectorize(
    data: pl.DataFrame, vectorizer_params: dict, random_state: int
) -> tuple[TfidfVectorizer, pl.DataFrame, pl.DataFrame]:
    tfidf_vectorizer = TfidfVectorizer(**vectorizer_params)

    train, val = train_test_split(
        data,
        test_size=0.3,
        shuffle=True,
        random_state=random_state,
    )
    tfidf_vectorizer.fit(train["corpus"].list.join(" ").to_numpy())
    return tfidf_vectorizer, train, val


def apply_vectorizer(vectorizer: TfidfVectorizer, data: pl.DataFrame) -> pl.DataFrame:
    return vectorizer.transform(data["corpus"].list.join(" ").to_numpy())


def batch_inference(batch, tokenizer, bert_model, device):
    tokenized_batch = tokenizer(
        batch, padding=True, truncation=True, return_tensors="pt"
    ).to(device)
    with torch.no_grad():
        hidden_batch = bert_model(**tokenized_batch)
        batch_embeddings = hidden_batch.last_hidden_state[:, 0, :].detach().to("cpu")
        return batch_embeddings


def train_bert_tokenizer(data: pl.DataFrame, random_state: int, model_name, fixed_batch_size):
    train, test = train_test_split(
        data,
        test_size=0.3,
        shuffle=True,
        random_state=random_state,
    )

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    tokenizer = AutoTokenizer.from_pretrained(model_name)
    bert_model = AutoModel.from_pretrained(model_name).to(device)

    train_dataloader = DataLoader(
        train["Review"].to_list(), batch_size=fixed_batch_size, shuffle=False
    )
    test_dataloader = DataLoader(
        test["Review"].to_list(), batch_size=fixed_batch_size, shuffle=False
    )

    train_embeddings = torch.concat(
        [batch_inference(batch_data, tokenizer, bert_model, device) for batch_data in train_dataloader]
    )
    test_embeddings = torch.concat(
        [batch_inference(batch_data, tokenizer, bert_model, device) for batch_data in test_dataloader]
    )
    return train_embeddings, test_embeddings, train, test
