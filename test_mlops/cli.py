import click
from clearml import TaskTypes
from clearml.automation.controller import PipelineDecorator


@PipelineDecorator.component(
    return_values=["data"],
    task_type=TaskTypes.data_processing,
)
def get_data():
    import polars as pl
    from clearml import Dataset, Task

    frame_path = Dataset.get(
        dataset_name="Raw data", dataset_project="Amazon reviews"
    ).get_local_copy()
    task = Task.current_task()
    task.set_progress(0)
    data = pl.read_csv(
        frame_path + "/train.csv",
        has_header=False,
        new_columns=["Polarity", "Title", "Review"],
        n_rows=5000,
    )
    task.set_progress(10)
    return data


@PipelineDecorator.component(
    return_values=["processed_data"],
    task_type=TaskTypes.data_processing,
)
def preprocessing(data, col_name):
    from clearml import Task
    from test_mlops.preprocessing import dataframe_preprocessing

    task = Task.current_task()
    processed_data = dataframe_preprocessing(data, col_name)
    task.set_progress(20)
    task.upload_artifact(name="processed_data", artifact_object=processed_data)
    return processed_data


@PipelineDecorator.component(
    return_values=["train_data", "train_result", "test_data", "test_result"],
    cache=True,
    task_type=TaskTypes.training,
)
def vectorize(data, vectorizer_params: dict, random_state: int):
    from clearml import Task
    import joblib
    from test_mlops.vectorize import apply_vectorizer, train_vectorize

    task = Task.current_task()
    vectorizer, train_data, test_data = train_vectorize(data, vectorizer_params, random_state)
    joblib.dump(vectorizer, "models/vectorizer.pkl", compress=True)

    train_result = apply_vectorizer(vectorizer, train_data)
    test_result = apply_vectorizer(vectorizer, test_data)
    task.set_progress(50)
    task.upload_artifact(
        name="train_features",
        artifact_object=(train_result, train_data["Polarity"].to_numpy()),
    )
    task.upload_artifact(
        name="test_features",
        artifact_object=(test_result, test_data["Polarity"].to_numpy()),
    )
    return train_data, train_result, test_data, test_result


@PipelineDecorator.component(
    return_values=["model"],
    cache=True,
    task_type=TaskTypes.training,
)
def train_model_tfidf(train_data, train_result, random_state, lr_multi_class, lr_solver):
    from clearml import Task
    import joblib
    from test_mlops.model import train

    task = Task.current_task()
    model = train(
        train_result,
        train_data["Polarity"].to_numpy(),
        {
            "random_state": random_state,
            "multi_class": lr_multi_class,
            "solver": lr_solver,
        },
    )
    joblib.dump(model, "models/model.pkl", compress=True)
    task.set_progress(80)
    return model


@PipelineDecorator.component(
    return_values=["result", "confusion"],
    task_type=TaskTypes.testing,
)
def test_model_tfidf(model, test_data, test_result):
    from clearml import Task
    from test_mlops.model import test

    task = Task.current_task()
    result, confusion = test(model, test_result, test_data["Polarity"].to_numpy())
    task.set_progress(90)
    return result, confusion


@PipelineDecorator.component(
    task_type=TaskTypes.qc,
)
def log_results_tfidf(result, confusion):
    from clearml import Task

    task = Task.current_task()
    logger = task.get_logger()
    logger.report_single_value("accuracy", result.pop("accuracy"))
    for class_name, metrics in result.items():
        for metric, value in metrics.items():
            logger.report_single_value(f"{class_name}_{metric}", value)
    logger.report_confusion_matrix("confusion matrix", "ignored", matrix=confusion)


@PipelineDecorator.component(
    return_values=["train_embeddings", "test_embeddings", "train", "test"],
    task_type=TaskTypes.data_processing,
)
def get_bert_embeddings(data, random_state, model_name, fixed_batch_size):
    from test_mlops.vectorize import train_bert_tokenizer
    from clearml import Task

    train_embeddings, test_embeddings, train, test = train_bert_tokenizer(data, random_state, model_name, fixed_batch_size)

    task = Task.current_task()
    task.upload_artifact(
        name="train_embeddings",
        artifact_object=train_embeddings,
    )
    task.upload_artifact(
        name="test_embeddings",
        artifact_object=test_embeddings,
    )
    return train_embeddings, test_embeddings, train, test


@PipelineDecorator.component(
    return_values=["model"],
    cache=True,
    task_type=TaskTypes.training,
)
def train_model_bert(train_data, train_result, random_state, lr_multi_class, lr_solver):
    from clearml import Task
    import joblib
    from test_mlops.model import train

    task = Task.current_task()
    model = train(
        train_result,
        train_data["Polarity"].to_numpy(),
        {
            "random_state": random_state,
            "multi_class": lr_multi_class,
            "solver": lr_solver,
        },
    )
    joblib.dump(model, "models/model.pkl", compress=True)
    task.set_progress(80)
    return model


@PipelineDecorator.component(
    return_values=["result", "confusion"],
    task_type=TaskTypes.testing,
)
def test_model_bert(model, test_data, test_result):
    from clearml import Task
    from test_mlops.model import test

    task = Task.current_task()
    result, confusion = test(model, test_result, test_data["Polarity"].to_numpy())
    task.set_progress(90)
    return result, confusion


@PipelineDecorator.component(
    task_type=TaskTypes.qc,
)
def log_results_bert(result, confusion):
    from clearml import Task

    task = Task.current_task()
    logger = task.get_logger()
    logger.report_single_value("accuracy", result.pop("accuracy"))
    for class_name, metrics in result.items():
        for metric, value in metrics.items():
            logger.report_single_value(f"{class_name}_{metric}", value)
    logger.report_confusion_matrix("confusion matrix", "ignored", matrix=confusion)


@click.command()
@click.argument("vec_max_feature", type=int, default=1000)
@click.argument("vec_analyzer", type=str, default="word")
@click.argument("random_state", type=int, default=42)
@click.argument("lr_multi_class", type=str, default="multinomial")
@click.argument("lr_solver", type=str, default="saga")
@click.argument("model_name", type=str, default="bert-base-uncased")
@click.argument("fixed_batch_size", type=int, default=128)
@PipelineDecorator.pipeline(
    name="Get Metrics Pipeline",
    project="Amazon reviews",
    version="1.0.0",
    output_uri=True
)
def cli_clearml(
    vec_max_feature: int,
    vec_analyzer: str,
    random_state: int,
    lr_multi_class: str,
    lr_solver: str,
    model_name: str,
    fixed_batch_size: int
):
    data = get_data()
    
    processed_data = preprocessing(data, "Review")
    train_data_tfidf, train_result, test_data_tfidf, test_result = vectorize(processed_data, 
                                                                             {"max_features": vec_max_feature, "analyzer": vec_analyzer},
                                                                             random_state)
    train_embeddings, test_embeddings, train_bert, test_bert = get_bert_embeddings(data, random_state, model_name, fixed_batch_size)

    model_tfidf = train_model_tfidf(train_data_tfidf, train_result, random_state, lr_multi_class, lr_solver)
    model_bert = train_model_bert(train_bert, train_embeddings, random_state, lr_multi_class, lr_solver)

    result_tfidf, confusion_tfidf = test_model_tfidf(model_tfidf, test_data_tfidf, test_result)
    result_bert, confusion_bert = test_model_bert(model_bert, test_bert, test_embeddings)

    log_results_tfidf(result_tfidf, confusion_tfidf)
    log_results_bert(result_bert, confusion_bert)
