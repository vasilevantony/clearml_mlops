# clearml_mlops



## Getting started

1. Create micromamba environment: micromamba create -f env.yaml
2. Activate environment: micromamba activate clearml  
3. Run ClearML server (in folder "infra"): docker compose up 